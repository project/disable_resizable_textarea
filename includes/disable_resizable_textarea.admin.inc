<?php
/**
 * @file
 * Codes for administration interface.
 */

/**
 * Form for advanced settings for Disable Resizable Textarea module.
 */
function disable_resizable_textarea_form($form, &$form_state) {
  // Load variable with custom options.
  $disable_resizable_textarea_options = variable_get('disable_resizable_textarea_options');

  $form['default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable #resizable property of all textarea at once'),
    '#description' => t('NOTE: This option ignores the custom settings made ​​in each field.'),
    '#default_value' => isset($disable_resizable_textarea_options) ? $disable_resizable_textarea_options['default'] : FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler to the save advanced settings defined on the form.
 */
function disable_resizable_textarea_form_submit($form, &$form_state) {
  $options = array(
    'default' => $form_state['values']['default'],
  );

  // Set the variable with custom options.
  variable_set('disable_resizable_textarea_options', $options);

  // Display a message on screen.
  drupal_set_message(t('All changes are saved.'));
}
